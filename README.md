# Requirements and Installers:

* Download and install npm. the version recommended for your OS: my version is v10.16.0 (https://nodejs.org/en/download/).
* Install globally  angular-cli: `npm install -g @angular/cli@latest` (last version). On Mac you have to write ‘sudo npm install -g @angular/cli@latest This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.1.
* Navigate to your prefer path: and download the project with this command: (git clone https://mauriciomage@bitbucket.org/mauriciomage/axa_test.git)
* navigate to the current project folder: cd axa_test and there, execute: `npm install` for install all the dependencies.
* Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
* enjoy the application!

# Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
