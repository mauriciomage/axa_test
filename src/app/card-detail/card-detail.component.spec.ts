import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDetailComponent } from './card-detail.component';
import {
  MatCard,
  MatCardTitle,
  MatCardSubtitle,
  MatCardHeader,
  MatCardFooter,
  MatListItem,
  MatList,
  MatCardContent
} from '@angular/material';

describe('CardDetailComponent', () => {
  let component: CardDetailComponent;
  let fixture: ComponentFixture<CardDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CardDetailComponent,
        MatCard,
        MatCardHeader,
        MatCardFooter,
        MatCardTitle,
        MatCardSubtitle,
        MatListItem,
        MatList,
        MatCardContent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
