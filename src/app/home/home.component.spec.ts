import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import {
  MatIcon,
  MatFormField,
  MatTable,
  MatChip,
  MatChipList,
  MatHeaderRow,
  MatHeaderRowDef,
  MatRowDef,
  MatPaginator
} from '@angular/material';
import { MatPaginatedTabHeader } from '@angular/material/tabs/typings/paginated-tab-header';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        MatIcon,
        MatFormField,
        MatTable,
        MatChip,
        MatChipList,
        MatHeaderRow,
        MatHeaderRowDef,
        MatRowDef,
        MatPaginator
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
