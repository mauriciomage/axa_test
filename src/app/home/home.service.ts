import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError} from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { BrastlewarkModel } from './home.interface';

@Injectable()
export class HomeService {

  constructor(private http: HttpClient) {}

  getDataBrastlwark(): Observable<BrastlewarkModel[]> {
    return this.http.get<any>(environment.apiUrl)
      .pipe(
        map(result => result.Brastlewark)
      );
  }
}
