import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog } from '@angular/material';
import { BrastlewarkModel } from './home.interface';
import { HomeService } from './home.service';
import { CardDetailComponent } from '../card-detail/card-detail.component';
import { Constants } from '../../shared/constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title: string;
  paginatorLength: number;
  pageSize: number;
  pageSizeOption: any[];
  brastlewarkData: BrastlewarkModel[];
  displayedColumns: string[] = ['id', 'name', 'age', 'weight', 'height', 'professions', 'hair_color', 'info'];
  dataSource;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;


  constructor(
    private homeService: HomeService,
    private dialog: MatDialog) {

    this.brastlewarkData = [];
    this.title = 'Welcome to Brastlewark!';
    this.pageSizeOption = Constants.pageSizeOptions;
    this.pageSize = Constants.pageSize;
  }


  ngOnInit() {
    this.homeService.getDataBrastlwark().subscribe((resp) => {
      if (resp) {
        this.brastlewarkData = resp;
        this.paginatorLength = this.brastlewarkData.length;
        this.dataSource = new MatTableDataSource<BrastlewarkModel>(this.brastlewarkData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  onClickMoreInfo(element) {
    this.dialog.open(CardDetailComponent, {
      width: 'auto',
      height: 'auto',
      data: element
    });
  }
}
