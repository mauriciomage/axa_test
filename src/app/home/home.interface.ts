export interface BrastlewarkModel {
    id: number;
    name: string;
    thumbnail: string;
    age: number;
    weight: number;
    height: number;
    hair_color: number;
    professions: any[];
    friends: any[];
  }
